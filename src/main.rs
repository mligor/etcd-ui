extern crate config;
extern crate serde;
extern crate stderrlog;

use etcd_rs::*;
use log::*;
use std::str;
use stderrlog::ColorChoice;
use warp::{http::StatusCode, Filter};

mod settings;

use settings::Settings;

#[tokio::main]
pub async fn main() {
    let settings = Settings::new();

    stderrlog::new()
        .module(module_path!())
        .verbosity(4)
        .color(ColorChoice::AlwaysAnsi)
        .init()
        .unwrap();

    trace!("{:?}", settings);

    let health_route = warp::path!("health")
        .map(|| StatusCode::OK)
        .with(warp::cors().allow_any_origin());

    let static_route = warp::any().and(warp::fs::dir("static-files"));

    let routes = static_route.or(health_route);

    let _r = connect_to_client().await;

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}

async fn connect_to_client() -> Result<()> {
    let settings = Settings::new()?;

    info!("Connecting to etcd server {}", settings.etcd);

    let client = Client::connect(ClientConfig {
        endpoints: vec![settings.etcd.to_owned()],
        auth: None,
    })
    .await?;

    info!("Listing all keys from etcd server:");

    let mut resp = client
        .kv()
        .range(RangeRequest::new(KeyRange::all()))
        .await?;

    for kv in resp.take_kvs() {
        info!(
            "  {} = {}",
            str::from_utf8(kv.key())?,
            str::from_utf8(kv.value())?
        );
    }

    Ok(())
}
