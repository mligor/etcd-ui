use config::*;
use serde::*;

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct Settings {
    pub debug: bool,
    pub database: String,
    pub etcd: String,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            debug: false,
            database: String::from("mongodb://localhost:27017/etcd-ui"),
            etcd: String::from("http://localhost:2379"),
        }
    }
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();

        // Start off by merging in the "default" configuration file
        s.merge(File::with_name("config").required(false))?;

        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
        s.merge(Environment::with_prefix("app"))?;

        // You may also programmatically change settings
        //s.set("database.url", "postgres://")?;

        // Now that we're done, let's access our configuration
        //println!("debug: {:?}", s.get_bool("debug"));
        //println!("database: {:?}", s.get::<String>("database.url"));
        //println!("etcd: {:?}", s.get::<String>("etcd.url"));

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_into()
    }
}
